Trip Fund 1.x for Drupal 7.x
----------------------------

This module allows members of organization to track fundraising efforts for
trips. Originally developed for use with church mission trips, this could
also be applied to student groups, community organizations, or any group
with similar needs.

Group leaders will be responsible for posting individual donations as
they are received so that participants can view their balance and progress.
Participants will be able to view all details of the donations they have
received so they can write thank you letters to contributors.

Installation
------------

Trip Fund can be installed like any other Drupal module -- place it in
the sites/all/modules/contrib directory for your site and enable it.
