<?php

/**
 * @file
 * User page callback file for the trip_fund_contributions module.
 *
 * @see trip_fund_coontributions.module
 */

/**
 * Page callback for the administration page.
 */
function trip_fund_contributions_admin_page() {
  $output = array();
  $output[] = array(
    '#type' => 'item',
    '#markup' => t('Administration page for contributions.'),
  );
  $output[] = array(
    '#type' => 'item',
    '#markup' => l(t('Add contribution'), 'contributions/add'),
  );
  $output['table'] = trip_fund_contributions_list_entities();
  return $output;
}

/**
 * Returns a render array with all trip_fund_trips entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function trip_fund_contributions_list_entities($uid = NULL, $trip_id = NULL) {
  $content = array();
  $matches = array();
  if (!empty($uid)) {
    $matches['uid'] = $uid;
  }
  if (!empty($trip_id)) {
    $matches['trip_id'] = $trip_id;
  }
  // Load all of our contributions.
  $contributions = trip_fund_contributions_load_multiple();
  if (!empty($contributions)) {
    foreach ( $contributions as $contribution ) {
      // Create tabular rows for our contributions.
      if ((empty($matches['uid']) || $matches['uid'] == $contribution->uid) && (empty($matches['trip_id']) || $matches['trip_id'] == $contribution->trip_id)) {
        $participant = user_load($contribution->uid);
        $trip = trip_fund_trips_load($contribution->trip_id);

        $actions = array();
        if (entity_access('view', 'trip_fund_contributions', $contribution)) {
          $actions[] = l(t('view'), 'contributions/' . $contribution->contrib_id);
        }
        if (entity_access('update', 'trip_fund_contributions', $contribution)) {
          $actions[] = l(t('edit'), 'contributions/' . $contribution->contrib_id . '/edit');
        }
        if (entity_access('delete', 'trip_fund_contributions', $contribution)) {
          $actions[] = l(t('delete'), 'contributions/' . $contribution->contrib_id . '/delete');
        }
        $rows[] = array(
          'data' => array(
            'date' => format_date($contribution->created_date, 'custom', 'n/j/Y'), // todo: Add localization support.
            'name' => $contribution->name,
            'participant' => l($participant->name, 'user/' . $contribution->uid . '/contributions'),
            'trip' => l($trip->title, 'trips/' . $contribution->trip_id),
            'amount' => number_format($contribution->amount, 2), // todo: Add localization support.
            'actions' => implode(' ', $actions),
          ),
        );
      }
    }
  }
  if (!empty($rows)) {
    // Put our contributions into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('Date'), t('Name'), t('Participant'), t('Trip'), t('Amount'), t('Actions')),
    );
  }
  else {
    // There were no contributions. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No contributions currently exist.'),
    );
  }
  return $content;
}

/**
 * Displays the entity item.
 */
function trip_fund_contributions_page_view($entity, $view_mode = 'full') {
  $participant = user_load($entity->uid);
  drupal_set_title(entity_label('trip_fund_contributions', $entity));
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l($participant->name, 'user/' . $entity->uid),
    l(t('Contributions'), 'user/' . $entity->uid . '/contributions')
  );
  drupal_set_breadcrumb($breadcrumb);
  return entity_view('trip_fund_contributions', array(entity_id('trip_fund_contributions', $entity) => $entity), $view_mode);
}

/**
 * Page callback for adding a contribution.
 */
function trip_fund_contributions_add($user = NULL, $trip = NULL) {
  drupal_set_title(t('Create Contribution'));
  $values = array();
  $context = 'any';
  if (!empty($user->uid)) {
    $values['uid'] = $user->uid;
    $context = 'user';
    drupal_set_title(t('Create contribution for @name', array('@name' => $user->name)));
  }
  if (!empty($trip->trip_id)) {
    $values['trip_id'] = $trip->trip_id;
    $context = 'trip';
    drupal_set_title(t('Create contribution for @title', array('@title' => $trip->title)));
  }

  $entity = entity_create('trip_fund_contributions', $values);
  return drupal_get_form('trip_fund_contributions_form', $entity, $context);
}

/**
 * Form to add/edit a new trip.
 */
function trip_fund_contributions_form($form, &$form_state, $contribution, $context = 'any') {
  // dsm($contribution, 'contribution');
  $form = array();

  // Make sure these ids are not empty to keep later code cleaner.
  if (empty($contribution->uid)) {
    $contribution->uid = 0;
  }
  if (empty($contribution->trip_id)) {
    $contribution->trip_id = 0;
  }

  // This exists for now because entity_example.module says so.
  $form['trip_fund_contributions'] = array(
    '#type' => 'value',
    '#value' => $contribution,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name associated with this contribution. (Donor or memo.)'),
    '#required' => TRUE,
    '#default_value' => $contribution->name,
  );

  switch ($context) {
    case 'any':
      // Avoid this usage. It should work, but is messy and not fun for the user.
      // Better to always use either the user or trip context than this free-for-all.
      $participants = trip_fund_participants_options(0, 0);
      $options = array();
      foreach ($participants as $key => $participant) {
        $options[$key] = $participant['name'] . ' - ' . $participant['title'];
      }
      asort($options);
      $form['pid'] = array(
        '#type' => 'select',
        '#title' => t('Trip assignment'),
        '#description' => t('Select the participant and trip for this contribution'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $contribution->pid,
      );
      break;
    case 'user':
      $participants = trip_fund_participants_options($contribution->uid, 0);
      $options = array();
      foreach ($participants as $key => $participant) {
        $options[$key] = $participant['title'];
      }
      asort($options);
      $form['pid'] = array(
        '#type' => 'select',
        '#title' => t('Trip'),
        '#description' => t('Select the trip for this contribution'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $contribution->pid,
      );
      break;
    case 'trip':
      $participants = trip_fund_participants_options(0, $contribution->trip_id);
      $options = array();
      foreach ($participants as $key => $participant) {
        $options[$key] = $participant['name'];
      }
      asort($options);
      $form['pid'] = array(
        '#type' => 'select',
        '#title' => t('Participant'),
        '#description' => t('Select the participant for this contribution'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $contribution->pid,
      );
      break;
  }

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#required' => TRUE,
    '#element_validate' => array('_element_validate_number'),
    '#default_value' => $contribution->amount,
  );

  field_attach_form('trip_fund_contributions', $contribution, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Validation for trip add form.
 */
function trip_fund_contributions_form_validate($form, &$form_state) {
  $values = (object) $form_state['values'];
  field_attach_form_validate('trip_fund_contributions', $values, $form, $form_state);
}

/**
 * Trip form submit.
 */
function trip_fund_contributions_form_submit($form, &$form_state) {
  $entity = $form_state['values']['trip_fund_contributions'];
  $entity->name = $form_state['values']['name'];
  $entity->pid = $form_state['values']['pid'];
  $entity->amount = $form_state['values']['amount'];

  // We store the uid and trip id of the participant record for easier database lookups.
  $assignment = trip_fund_participants_load($entity->pid);
  $entity->uid = $assignment->uid;
  $entity->trip_id = $assignment->trip_id;

  field_attach_submit('trip_fund_contributions', $entity, $form, $form_state);
  $entity = trip_fund_contributions_save($entity);
  $form_state['redirect'] = 'contributions/' . $entity->contrib_id;
}

/**
 * Page callback for an individual user's trip transactions list.
 */
function trip_fund_contributions_user_list($account) {
  $output = array();
  $assignments = trip_fund_participants_load_multiple(FALSE, array('uid' => $account->uid), TRUE);
  if (empty($assignments)) {
    $output[] = array(
      '#type' => 'item',
      '#markup' => t('User is not assigned to any trips.'),
    );
  }
  else {
    $is_manager = trip_fund_user_manager_access($account);
    if ($is_manager) {
      //todo: Also add check to make sure current user is assigned to trips before allowing contributions.
      $output[] = array(
        '#type' => 'item',
        '#markup' => l(t('Add contribution'), 'user/' . $account->uid . '/contributions/add'),
      );
    }
    // dsm($assignments);

    foreach ( $assignments as $assignment ) {
      $trip = trip_fund_trips_load($assignment->trip_id);
      $info = array(
        'title' => array(
          '#prefix' => '<h3 class="title">',
          '#markup' => t('Contribution history for @title', array('@title' => $trip->title)),
          '#suffix' => '</h3>',
        ),
        'amount_due' => array(
          '#prefix' => '<p>',
          '#markup' => t('Total amount: $@amount', array('@amount' => $assignment->total_due)),
          '#suffix' => '</p>',
        ),
      );

      $conditions = array('uid' => $account->uid, 'trip_id' => $assignment->trip_id);
      $contributions = trip_fund_contributions_load_multiple(FALSE, $conditions, TRUE);
      if (!empty($contributions)) {
        $rows = array();
        $balance = intval($assignment->total_due);
        foreach ( $contributions as $contribution ) {
          $actions = array();
          if (entity_access('view', 'trip_fund_contributions', $contribution)) {
            $actions[] = l(t('view'), 'contributions/' . $contribution->contrib_id);
          }
          if (entity_access('update', 'trip_fund_contributions', $contribution)) {
            $actions[] = l(t('edit'), 'contributions/' . $contribution->contrib_id . '/edit');
          }
          if (entity_access('delete', 'trip_fund_contributions', $contribution)) {
            $actions[] = l(t('delete'), 'contributions/' . $contribution->contrib_id . '/delete');
          }
          $balance = $balance - $contribution->amount;
          $rows[] = array(
            'data' => array(
              'date' => format_date($contribution->created_date, 'custom', 'n/j/Y'), // todo: Add localization support.
              'name' => $contribution->name,
              // 'participant' => l($account->name, 'user/' . $contribution->uid . '/contributions'),
              // 'trip' => l($trip->title, 'trips/' . $contribution->trip_id),
              'amount' => number_format($contribution->amount, 2), // todo: Add localization support.
              'balance' => number_format($balance, 2), // todo: Add localization support.
              'actions' => implode(' ', $actions),
            ),
          );
        }
      }
      if (!empty($rows)) {
        // Put our contributions into a themed table. See theme_table() for details.
        $info['detail'] = array(
          '#theme' => 'table',
          '#rows' => $rows,
          '#header' => array(t('Date'), t('Name'), t('Amount'), t('Balance'), t('Actions')),
        );
        $info['balance'] = array(
          '#prefix' => '<p>',
          '#markup' => t('Remaining balance: $@balance', array('@balance' => number_format($balance, 2))),
          '#suffix' => '</p>',
        );
      }
      else {
        // There were no contributions. Tell the user.
        $info[] = array(
          '#type' => 'item',
          '#markup' => t('No contributions currently exist.'),
        );
      }
      $output[] = $info;
    }
  }
  return $output;
}
