<?php

/**
 * @file
 * Interface and Controller for trip_fund_Contributions entities.
 */

/**
 * TripFundContributions class.
 */
class TripFundContributions extends Entity {
  protected function defaultLabel() {
    return t('Contribution') . ' ' . $this->identifier();
  }
  protected function defaultUri() {
    return array('path' => 'contributions/' . $this->identifier());
  }
}

/**
 * TripFundContributionsController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class TripFundContributionsController extends EntityAPIController {

  /**
   * Create and return a new trip_fund_trips entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'contrib_id' => 0,
      'name' => '',
      'pid' => 0,
      'amount' => 0,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // dsm($entity, 'entity');
    $wrapper = entity_metadata_wrapper('trip_fund_Contributions', $entity);

    $content['name'] = array(
      '#theme' => 'field',
      '#weight' => -10,
      '#title' => t('Name'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'display_contribution_name',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_contributions',
      '#bundle' => 'trip_fund_contributions',
      '#items' => array(array('value' => $entity->name)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->name))
    );

    $content['amount'] = array(
      '#theme' => 'field',
      '#weight' => -8,
      '#title' => t('Amount'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'display_contribution_cost',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_contributions',
      '#bundle' => 'trip_fund_contributions',
      '#items' => array(array('value' => $entity->amount)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->amount))
    );

    $participant = user_load($entity->uid);
    $trip = trip_fund_trips_load($entity->trip_id);

    $content['participant'] = array(
      '#theme' => 'field',
      '#weight' => -7,
      '#title' => t('Participant'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'display_contribution_participant',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_contributions',
      '#bundle' => 'trip_fund_contributions',
      '#items' => array(array('value' => $participant->name)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => l($participant->name, 'user/' . $entity->uid . '/contributions')),
    );

    $content['trip'] = array(
      '#theme' => 'field',
      '#weight' => -6,
      '#title' => t('Trip'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'display_contribution_trip',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_contributions',
      '#bundle' => 'trip_fund_contributions',
      '#items' => array(array('value' => $trip->title)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => l($trip->title, 'trips/' . $entity->trip_id)),
    );

    $user = user_load($entity->created_uid);
    $submitted = format_date($entity->created_date) . ' by ' . $user->name;

    $content['submitted'] = array(
      '#theme' => 'field',
      '#weight' => 10,
      '#title' => t('Submitted'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'display_contribution_submitted',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_contributions',
      '#bundle' => 'trip_fund_contributions',
      '#items' => array(array('value' => $submitted)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($submitted))
    );

    if ($entity->created_date !== $entity->changed_date) {
      $user = user_load($entity->changed_uid);
      $updated = format_date($entity->changed_date) . ' by ' . $user->name;

      $content['updated'] = array(
        '#theme' => 'field',
        '#weight' => 11,
        '#title' => t('Updated'),
        '#access' => TRUE,
        '#label_display' => 'inline',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'display_contribution_updated',
        '#field_type' => 'text',
        '#entity_type' => 'trip_fund_contributions',
        '#bundle' => 'trip_fund_contributions',
        '#items' => array(array('value' => $updated)),
        // todo: Make this numeric.
        '#formatter' => 'text_default',
        0 => array('#markup' => check_plain($updated))
      );
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
