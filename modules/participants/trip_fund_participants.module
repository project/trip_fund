<?php

/**
 * @file
 * Drupal module: Trip fund participants.
 *
 * This module contains the code necessary to manage the "participants" entity type
 * for the Trip Fund project.
 *
 * @see trip_fund.module
 * @see trip_fund_trips.module
 * @see trip_fund_contributions.module
 */

/**
 * Implements hook_menu().
 */
function trip_fund_participants_menu() {
  $items = array();
  $items['user/%user/trips'] = array(
    'title' => 'Trip Activity',
    'page callback' => 'trip_fund_participants_user_trips',
    'page arguments' => array(1),
    'access callback' => 'trip_fund_user_view_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'trip_fund_participants.pages.inc',
  );
  $items['user/%user/trips/%trip_fund_participants/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('trip_fund_participants_edit_form', 1, 3),
    'access callback' => 'entity_access',
    'access arguments' => array('update', 'trip_fund_participants', 3),
    'type' => MENU_CALLBACK,
    'file' => 'trip_fund_participants.pages.inc',
  );
  $items['user/%user/trips/%trip_fund_participants/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('trip_fund_participants_delete_form', 1, 3),
    'access callback' => 'entity_access',
    'access arguments' => array('delete', 'trip_fund_participants', 3),
    'type' => MENU_CALLBACK,
    'file' => 'trip_fund_participants.pages.inc',
  );
  return $items;
}

/**
 * Implements hook_entity_info().
 *
 * @see http://www.istos.it/blog/drupal-entities/drupal-entities-part-3-programming-hello-drupal-entity
 * @see http://drupal.org/node/1026420
 */
function trip_fund_participants_entity_info() {
  return array(
    'trip_fund_participants' => array(
      'label' => t('Participants'),
      'entity class' => 'TripFundParticipants',
      'controller class' => 'TripFundParticipantsController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'trip_fund_participants',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'trip_fund_participants_access',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'assignment_id',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'trip_fund_participants' => array(
          'label' => 'Participants',
          // 'admin' => array(
          //   'path' => 'admin/structure/trips/manage',
          //   'access arguments' => array('manage trip contributions'),
          // ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Trip Details'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Access callback for trip assignments.
 *
 * Arguments for this function are defined by entity_access().
 * @see entity_access()
 */
function trip_fund_participants_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {
  switch ($op) {
    case 'view':
      if (isset($entity)) {
        return trip_fund_user_view_access($entity->uid);
      }
      break;
    case 'create':
    case 'update':
    case 'delete':
      return user_access('manage trip contributions', $account);
  }
}

/**
 * Load a single record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function trip_fund_participants_load($assignment_id = NULL, $reset = FALSE) {
  $assignment_ids = (isset($assignment_id) ? array($assignment_id) : array());
  $trip_fund_participants = trip_fund_participants_load_multiple($assignment_ids, $reset);
  return $trip_fund_participants ? reset($trip_fund_participants) : FALSE;
}

/**
 * Load multiple records.
 */
function trip_fund_participants_load_multiple($assignment_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('trip_fund_participants', $assignment_ids, $conditions, $reset);
}

/**
 * We save the entity by calling the controller.
 */
function trip_fund_participants_save(&$entity) {
  return entity_save('trip_fund_participants', $entity);
}

/**
 * Delete single assignment.
 */
function trip_fund_participants_delete($entity) {
  entity_delete('trip_fund_participants', entity_id('trip_fund_participants', $entity));
}

/**
 * Delete multiple assignments.
 */
function trip_fund_participants_delete_multiple($assignment_ids) {
  entity_delete_multiple('trip_fund_participants', $assignment_ids);
}

/**
 * Returns a render array with all trip_fund_participants entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function trip_fund_participants_list_entities($uid = NULL, $trip_id = NULL) {
  $content = array();
  // Load all of our entities.
  $matches = array();
  if (!empty($uid)) {
    $matches['uid'] = $uid;
  }
  if (!empty($trip_id)) {
    $matches['trip_id'] = $trip_id;
  }
  $assignments = trip_fund_participants_load_multiple(FALSE, $matches, TRUE);
  if (!empty($assignments)) {
    foreach ( $assignments as $assignment ) {
      $trip = trip_fund_trips_load($assignment->trip_id);

      $actions = array();
      if (entity_access('update', 'trip_fund_participants', $assignment)) {
        $actions[] = l(t('edit'), 'user/' . $assignment->uid . '/trips/' . $assignment->assignment_id . '/edit');
      }
      if (entity_access('delete', 'trip_fund_participants', $assignment)) {
        $actions[] = l(t('delete'), 'user/' . $assignment->uid . '/trips/' . $assignment->assignment_id . '/delete');
      }
      // Create table row for this assignments.
      $rows[] = array(
        'data' => array(
          'title' => l($trip->title, 'trips/' . $assignment->trip_id),
          'trip_cost' => number_format($assignment->total_due, 2), // todo: Add localization support.
          'actions' => implode(' ', $actions),
        ),
      );
    }
    // Put our assignments into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('Trip'), t('Total Due'), t('Actions')),
    );
  }
  else {
    // There were no assignments. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No trip assignments currently exist.'),
    );
  }
  return $content;
}

/**
 * Returns an unformatted array of trip_fund_participants entities
 * for use when assiging contribution to participants.
 */
function trip_fund_participants_options($uid = NULL, $trip_id = NULL) {
  $rows = array();
  $users = array();
  $trips = array();
  // Load all of our entities.
  $matches = array();
  if (!empty($uid) && $uid !== 0) {
    $matches['uid'] = $uid;
  }
  if (!empty($trip_id) && $trip_id !== 0) {
    $matches['trip_id'] = $trip_id;
  }
  $assignments = trip_fund_participants_load_multiple(FALSE, $matches, TRUE);
  if (!empty($assignments)) {
    foreach ( $assignments as $assignment ) {
      if (empty($users[$assignment->uid])) {
        $user = user_load($assignment->uid);
        $users[$assignment->uid] = $user->name;
      }
      if (empty($trips[$assignment->trip_id])) {
        $trip = trip_fund_trips_load($assignment->trip_id);
        $trips[$assignment->trip_id] = $trip->title;
      }
      $rows[$assignment->assignment_id] = array(
        'uid' => $assignment->uid,
        'name' => $users[$assignment->uid],
        'trip_id' => $assignment->trip_id,
        'title' => $trips[$assignment->trip_id],
      );
    }
  }
  return $rows;
}
