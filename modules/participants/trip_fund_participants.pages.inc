<?php

/**
 * @file
 * User page callback file for the trip_fund_participants module.
 *
 * @see trip_fund_participants.module
 */

/**
 * Page callback for the trip participant list.
 */
function trip_fund_participants_user_trips($account) {
  $is_manager = trip_fund_user_manager_access($account);
  $output = array();
  $assignments = trip_fund_participants_load_multiple(FALSE, array('uid' => $account->uid), TRUE);
  $output['trip_participant_list'] = array(
    '#type' => 'item',
    '#title' => t('Currently assigned trips'),
    'child' => trip_fund_participants_list_entities($account->uid),
  );

  if ($is_manager) {
    $trips = array();
    foreach ($assignments as $assignment) {
      $trips[$assignment->trip_id] = $assignment->trip_id;
    }
    $account->trips = $trips;
    $output[] = drupal_get_form('trip_fund_participants_add_trip', $account);
  }

  return $output;
}

/**
 * Display form for adding participants to trips.
 */
function trip_fund_participants_add_trip($form, &$form_state, $account = array()) {
  $form = array();
  $trips = trip_fund_trips_load_multiple();
  $options = array();
  foreach ($trips as $trip) {
    if (empty($account->trips[$trip->trip_id])) {
      $options[$trip->trip_id] = $trip->title . ' (' . $trip->trip_cost . ')';
    }
  }
  if (!empty($options)) {
    $form['uid'] = array(
      '#type' => 'hidden',
      '#value' => $account->uid,
    );
    $form['trip'] = array(
      '#type' => 'select',
      '#title' => t('Available trips'),
      '#options' => $options
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add trip'),
      '#weight' => 100,
    );
  }
  else {
    $form['notrip'] = array(
      '#type' => 'markup',
      '#markup' => t('There are no trips available to add.'),
    );
  }
  return $form;
}

/**
 * Validate trip addition form.
 */
function trip_fund_participants_add_trip_validate($form, &$form_state) {
  // Ensure user can be assigned to trips. This should never fail.
  $uid = $form_state['values']['uid'];
  $account = user_load($uid);
  if (!user_access('be assigned to trips', $account)) {
    form_set_error('uid', t('Cannot assign user @name to trips.', array('@name' => $account->name)));
  }
  else {
    $form_state['values']['name'] = $account->name;
  }

  // Make sure user is not already on this trip.
  $trip_id = $form_state['values']['trip'];
  $matches = array('uid' => $uid, 'trip_id' => $trip_id);
  $assignment = trip_fund_participants_load_multiple(FALSE, $matches, TRUE);
  if (!empty($assignment)) {
    form_set_error('trip', t('User is already on this trip. Please select a different trip.'));
  }

  // Make sure we can read the cost of this trip.
  $trip = trip_fund_trips_load($trip_id);
  if (empty($trip->trip_cost)) {
    form_set_error('trip', t('Cannot load trip cost. Please select a different trip.'));
  }
  else {
    $form_state['values']['cost'] = $trip->trip_cost;
    $form_state['values']['title'] = $trip->title;
  }
}

/**
 * Form submission handler for adding a trip.
 */
function trip_fund_participants_add_trip_submit($form, &$form_state) {
  $values = array(
    'uid' => $form_state['values']['uid'],
    'trip_id' => $form_state['values']['trip'],
    'total_due' => $form_state['values']['cost'],
  );
  $entity = entity_create('trip_fund_participants', $values);
  $entity = trip_fund_participants_save($entity);
}

/**
 * Display a form for editing trip dollar amounts.
 */
function trip_fund_participants_edit_form($form, &$form_state, $user, $assignment) {
  $trip = trip_fund_trips_load($assignment->trip_id);
  drupal_set_title(t('Edit trip assignment @trip for @user', array('@trip' => $trip->title, '@user' => $user->name)));
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l($user->name, 'user/' . $user->uid),
    l(t('Trips'), 'user/' . $user->uid . '/trips')
  );
  drupal_set_breadcrumb($breadcrumb);

  dsm($assignment, 'assignment');

  $form = array();
  $form['construction'] = array(
    '#markup' => t('Under construction.'),
  );
  return $form;
}
