<?php

/**
 * @file
 * Interface and Controller for trip_fund_participants entities.
 */

/**
 * TripFundParticipants class.
 */
class TripFundParticipants extends Entity {
  protected function defaultLabel() {
    return t('Assignment') . ' ' . $this->identifier();
  }
  protected function defaultUri() {
    return array('path' => 'trips/participants/' . $this->identifier());
  }
}

/**
 * TripFundParticipantsController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class TripFundParticipantsController extends EntityAPIController {

  /**
   * Create and return a new trip_fund_trips entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'assignment_id' => 0,
      'uid' => 0,
      'trip_id' => 0,
      'total_due' => 0,
    );
    return parent::create($values);
  }

/*
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('trip_fund_participants', $entity);
    $content['total_due'] = array(
      '#theme' => 'field',
      '#weight' => -5,
      '#title' => t('Total due'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_trip_cost',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_participants',
      '#bundle' => 'trip_fund_participants',
      '#items' => array(array('value' => $entity->total_due)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->total_due))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
 */
}
