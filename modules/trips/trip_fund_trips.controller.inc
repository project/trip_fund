<?php

/**
 * @file
 * Interface and Controller for trip_fund_trips entities.
 */

/**
 * TripFundTrips class.
 */
class TripFundTrips extends Entity {
  protected function defaultLabel() {
    return $this->title;
  }
  protected function defaultUri() {
    return array('path' => 'trips/' . $this->identifier());
  }
}

/**
 * TripFundTripsController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class TripFundTripsController extends EntityAPIController {

  /**
   * Create and return a new trip_fund_trips entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'trip_id' => 0,
      'title' => '',
      'trip_cost' => '',
      'created_date' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('trip_fund_trips', $entity);
    // $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    // Make Description and Status themed like default fields.
    $content['trip_cost'] = array(
      '#theme' => 'field',
      '#weight' => -5,
      '#title' => t('Trip cost'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_trip_cost',
      '#field_type' => 'text',
      '#entity_type' => 'trip_fund_trips',
      '#bundle' => 'trip_fund_trips',
      '#items' => array(array('value' => $entity->trip_cost)),
      // todo: Make this numeric.
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->trip_cost))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
